<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0;" name="viewport" />
    <title>八零体系2019年度新春酒会</title>
    <link rel="stylesheet" type="text/css" href="/static/layui/css/layui.css">
    <link rel="stylesheet" type="text/css" href="/source/show/index.css">
    <script src="/static/js/jquery.min.js"></script>
</head>

<body>

    <div class="box">

        <div class="zhibojian-header">
            <img id="image" src="{{$program['now']['resource_phone']}}" alt=""  class="bg-img">
            {{--<img id="image" src="http://www.youqitui.com/upload/common/20181016/3c2b69354b1088a847e2bcfb997f37d6.png" alt="" id="zan" class="bg-img">--}}
            <div class="title-tab">
                <ul class="tab-content">
                    <li class="active">互动</li>
                    <li id="details">简介</li>
                    <li id="sort">排名</li>
                </ul>
            </div>
            <div class="title">
                <!-- <div class="now-txt scroll_begin">
                    <div>
                    正在播放:
                    </div>
                    <div class="animate">
                        <span id="now">【叫我我就打我就等我了解两位我就打我就文件夹端午节】</span>
                    </div>

                </div> -->

                <!-- <p class="next-txt">敬请期待节目: <span id="next">【发的纷纷额而非额而非额 额 分 费瓦非法】</span></p> -->

                <div class="title-left">
                    <div class="scroll-content">
                        <div class="scroll-title">
                            正在演出:
                        </div>
                        <div id="scroll_div" class="fl">
                            <div id="scroll_begin">
                                <span class="pad_right" id="now">{{$program['now']['program_type']}} : {{$program['now']['program_name']}}</span>
                            </div>
                            <div id="scroll_end"></div>
                        </div>

                    </div>


                    <div class="scroll-content">
                        <div class="scroll-title">
                        敬请期待:
                        </div>
                        <div id="scroll_div2" class="fl">
                            <div id="scroll_begin2">
                                <span class="pad_right" id="next">{{$program['next']['program_type']}} : {{$program['next']['program_name']}}</span>
                            </div>
                            <div id="scroll_end2"></div>
                        </div>

                    </div>
                </div>

                <div class="toupiao">
                    @if($is_vote)
                        <div class="zan-btn" style="pointer-events: none;">已投</div>
                    @else
                        <div class="zan-btn">投票</div>
                    @endif
                </div>

            </div>

        </div>
        <div class="zhibo-items">
            <div id="message" class="message">
                <!-- 互动 -->
                <div class="page1">
                    {{--<div class="welcome">--}}
                    {{--欢迎<span>朱贺</span>光临直播间--}}
                    {{--</div>--}}
                    {{----}}
                    {{--<p class="user-word">--}}
                    {{--<span class="name">朱贺撒谎i博士硕士 :</span>--}}
                    {{--内容四点九四大姐十九点三内容四点九四大姐十九点点九四大姐十九点三内容四点九四大姐十九点三--}}
                    {{--</p>--}}
                </div>
                <!-- 简介 -->
                <div class="page2">
                    {{--<p>名称: <span>大话西游之朱贺相亲记</span></p>--}}
                    {{--<p>演员: </p>--}}
                    {{--<p>--}}
                        {{--朱贺(沙和尚) 陈震宇(唐三藏)--}}
                    {{--</p>--}}
                    {{--<p>简介:</p> --}}
                    {{--<p>--}}
                    {{--话说。。。立刻就发啦速度快放假了卡时间段里疯狂--}}
                    {{--</p>--}}



                </div>
                <!-- 排名 -->
                <div class="page3">
                    {{--<div class='ranking-item'>--}}
                        {{--<div class="jiemu-title">--}}
                            {{--<p>大话西游</p >--}}
                            {{--<p> <span>1000</span> 票</p >--}}
                        {{--</div>--}}
                        {{--<div class="progress-content">--}}
                            {{--<div class="progress progress1"></div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                </div>


            </div>


            <div class="journal-reward"></div>
            <div id="autoClick" style="display: none;"></div>
            <div class="bottom">
                <!-- <button id="send"  class="layui-btn send">发送<button> -->
                <input type="text" id="content" maxlength="60"  placeholder="吐个槽呗~" autocomplete="off" style="width: 96%;" class="layui-input input-focus">
                <button id="send"  class="layui-btn send">发送</button>
                <!-- <div class="send" id="send">发送</div> -->
            </div>
        </div>

    </div>

    <div class="view-dialog-bg">
        <div class="view-dialog">
            <p> <strong>提示</strong></p >
            <p class="view-text">确定投一票吗?</p >
            <div class="view-sub">
                <div class="view-sub-cancel" >取消</div>
                <div class="view-sub-confirm"> 确定</div>
            </div>
        </div>
    </div>
</body>
<script>
</script>

<script src="/source/phone/queue.js"></script>
<script>
    var openid = "{{$info['openid']}}";
    var nickname = "{{$info['nickname']}}";
    var postZanUrl = "{{route('zan')}}";
    var sortUrl = "{{route('sort')}}";
    var detailsUrl = "{{route('details')}}";
    var voteUrl = "{{route('vote')}}";
    // var openid = "123123213123";
    // var nickname = "zhuhe";
    var queuelen = 10
    var queue = new Queue(queuelen);
    var tabItemIndex = 1; //记录点击哪一个tab
</script>

<script src="/static/layui/layui.js"></script>
<script src="/source/phone/index.js"></script>
<script src="/static/js/animation.js"></script>
<script>

    var layers = {   //弹框事件 layer.msg(内容)                    
        msg:function(msg){
            if($('#gmsg').length>0){
                return false
            }
            $('body').append('<div id="gmsg" ><a class="gmsg"></ a></div>')
            $('.gmsg').html(msg)
            $('#gmsg').css({
                position:'fixed',
                top:'40%',
                width:'100%',
                textAlign:'center'
            })
            $('.gmsg').css({
                padding:'6px 14px',
                fontSize:'16px',
                color:'#fff',
                opacity:'1',
                webkitOpacity:'1',
                textAlign:'center',
                background:'rgba(0,0,0,0.8)',
                borderRadius:'4px'
            })
            setTimeout(function(){
                $('#gmsg').remove()
            },2000)
        }
    }

    $('.view-dialog-bg').hide();
    $('.zan-btn').click(function() {
//点击点赞弹出框
        $('.view-dialog-bg').show();
    });
    $('.view-sub-cancel').click(function() {
//点击取消
        $('.view-dialog-bg').hide();
    });
    $('.view-sub-confirm').click(function() {
//点击确定
        $('.view-dialog-bg').hide();
        var program_id = window.sessionStorage.getItem("program_id");
        if(!program_id || program_id<= 0){
            layers.msg('当前节目不可投票');
            return;
        }

        $.ajax({
            url:voteUrl+'/'+program_id,
            type:'post',
            dataType:'json',
            success:function (res) {
                if (res.code == 0) {
                    layers.msg('已经投过票了!');
                    $(".zan-btn").html('已投');
                    $(".zan-btn").css('pointer-events', 'none');
                } else if (res.code == 2){
                    layers.msg('当前节目不可投票!');
                }else{
                    layers.msg('投票成功!');
                    $(".zan-btn").html('已投');
                    $(".zan-btn").css('pointer-events','none');

                    if (tabItemIndex == 3) {
                        setTimeout(()=>{
                            $("#sort").click();
                        },3000)
                    }
                }
            },
            error:function () {
                console.log('请求失败！')
            }
        })

    });
    




    $(function(){
        window.sessionStorage["program_id"] = "{{$program['now']['id']}}";
        $("#details").click(function () {
            var program_id = window.sessionStorage.getItem("program_id");
            if(!program_id || program_id<= 0){
                return;
            }
            $(".page2").html();
            $.ajax({
                url:detailsUrl+'/'+program_id,
                type:'get',
                dataType:'json',
                success:function (res) {
                    $(".page2").html(res.data.program_details);
                },
                error:function () {
                    console.log('请求失败！')
                }
            })
        });
        $("#sort").click(function () {
            $(".page3").html('');
            $.ajax({
                url:sortUrl+'?all=0',
                type:'get',
                dataType:'json',
                success:function (res) {
                    for (var i=0; i<res.data.length;i++){
                        var count = 0;
                        if(res.data[i].vote){
                            count = res.data[i].vote.count;
                        }
                        if(res.data[i].record.length == 0){
                            $(".page3").append(
                                "<div class='ranking-item'><div class='jiemu-title'><p>"+res.data[i].program_name+"</p ><p> <span>"+ count+"</span> 票 | <span id='toupiao"+res.data[i].id+"'  class='tou-btn' onclick='toupiao("+res.data[i].id+")'>投票</span></p ></div><div class='progress-content'><div class='progress progress"+(i+1)+"'></div></div></div>"
                            );
                        }else{
                            var program_id = window.sessionStorage.getItem("program_id");
                            if(program_id == res.data[i].id ){
                                $(".zan-btn").html('已投');
                                $(".zan-btn").css('pointer-events','none');
                            }
                            $(".page3").append(
                                "<div class='ranking-item'><div class='jiemu-title'><p>"+res.data[i].program_name+"</p ><p> <span>"+ count+"</span> 票 | <span id='toupiao"+res.data[i].id+"' class='tou-btn2'>已投</span></p ></div><div class='progress-content'><div class='progress progress"+(i+1)+"'></div></div></div>"
                            );
                        }
                        if(res.count == 0){
                            $(".progress"+(i+1)).css('width','0');
                        }else{
                            $(".progress"+(i+1)).css('width',(count/res.count)*100+'%');
                        }
                    }
                },
                error:function () {
                    console.log('请求失败！')
                }
            })
        });

        $(window).load(function() { //设置直接列表message的高度
            var bodyHeight = $(window).height();
            var headerHeight = $('.zhibojian-header').height();
            var bottomHeight = $('.bottom').height();
            $('#message').height(bodyHeight - headerHeight - bottomHeight);
            var hhh = $('body').height();

            $('.box').height(hhh);
            $('.page1').show().siblings().hide();
        });

         //文字横向滚动
        function ScrollImgLeft(index){
            var speed=50;
            var MyMar = null;
            var scroll_begin = document.getElementById("scroll_begin"+index);
            var scroll_end = document.getElementById("scroll_end"+index);
            var scroll_div = document.getElementById("scroll_div"+index);
            scroll_end.innerHTML=scroll_begin.innerHTML;
            function Marquee(){
            if(scroll_end.offsetWidth-scroll_div.scrollLeft<=0)
            scroll_div.scrollLeft-=scroll_begin.offsetWidth;
            else
            scroll_div.scrollLeft++;
            }
            MyMar=setInterval(Marquee,speed);
        }
        var scrollWidth = $('#scroll_div2').width();
        var nowWidth = $('#now').width();
        var nextWidth = $('#next').width();
        if (nowWidth>=scrollWidth) {
            ScrollImgLeft('');
        }
        if (nextWidth>=scrollWidth) {
            ScrollImgLeft('2');
        }


    })

    // tab切换
	
    $(".tab-content").on("click","li",function(){
		// 切换li列表样式
		$(this).addClass("active");
		$(this).siblings().removeClass("active");
		// 获取当前点击li 的下标
		var index = $(this).index() + 1;
        $('.page' + index).show().siblings().hide();
        if (index == 1) {
            $('#content').css('z-index','1000');
            $('#send').css('z-index','1000');
            $('.bottom').css('background','#fff');
        }else {
            $('.bottom').css('background','#F4F4F4');
            $('#content').css('z-index','-10');
            $('#send').css('z-index','-10');
        }

		if (index == 3) {
            $('canvas').hide();
            $('.journal-reward').hide();
			tabItemIndex = 3;
		} else if (index == 2){
            tabItemIndex = 2;
            $('canvas').show();
            $('.journal-reward').show();
        }else {
            $('canvas').show();
            $('.journal-reward').show();
			tabItemIndex = 1;
		}

    });


    $(document).on('blur','#content',function(){
        temporaryRepair()
    });
    function temporaryRepair(){
        var currentPosition,timer;
        var speed=1;//页面滚动距离
        timer=setInterval(function(){
            currentPosition=document.documentElement.scrollTop || document.body.scrollTop;
            currentPosition-=speed; 
            window.scrollTo(0,currentPosition);//页面向上滚动
            currentPosition+=speed; //speed变量
            window.scrollTo(0,currentPosition);//页面向下滚动
            clearInterval(timer);
        },1);
    }
    function toupiao(id) {
        var program_id = window.sessionStorage.getItem("program_id");
        if(id > program_id){
            layers.msg('节目暂未开始');
            return;
        }
        $.ajax({
            url:voteUrl+'/'+id,
            type:'post',
            dataType:'json',
            success:function (res) {
                if(res.code == 0){
                    layers.msg('已经投过票了!');
                }else {
                    layers.msg('投票成功!');
                    $("#toupiao"+id).html('已投');
                    var program_id = window.sessionStorage.getItem("program_id");
                    if(id == program_id){
                        $(".zan-btn").html('已投');
                        $(".zan-btn").css('pointer-events','none');
                    }
                    if (tabItemIndex == 3) {
                        setTimeout(()=>{
                            $("#sort").click();
                        },3000)
                    }
                }
            },
            error:function () {
                console.log('请求失败！')
            }
        })
    }

</script>
</html>
