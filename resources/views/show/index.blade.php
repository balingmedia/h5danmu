<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel</title>
    {{--<link rel="stylesheet" type="text/css" href="/static/css/barrager.css">--}}
    {{--<link rel="stylesheet" type="text/css" href="/static/pick-a-color/css/pick-a-color-1.2.3.min.css">--}}
    {{--<link type="text/css" rel="stylesheet" href="/static/syntaxhighlighter/styles/shCoreDefault.css"/>--}}
</head>
<body>
<canvas style="width: 100%;height: 100%;position: absolute;overflow: hidden;">你的浏览器不支持canvas</canvas>
</body>
<script>
    var openid = "{{env('SHOW_OPENID')}}";
</script>
<script src="/static/js/jquery.js"></script>
{{--<script type="text/javascript" src="/static/js/jquery.barrager.min.js"></script>--}}
<script src="/source/show/index2.js"></script>
<script src="/source/show/index.js"></script>
</html>
