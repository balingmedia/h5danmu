<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {http://barrage.balingmedia.com/
        Schema::create('programs', function (Blueprint $table) {
            $table->increments('id')->comment('节目表');
            $table->string('section',50)->comment('部门');
            $table->string('program_name',255)->comment('节目名称');
            $table->string('program_type',50)->comment('节目类型');
            $table->string('program_details',255)->comment('节目详情');
            $table->string('resource',255)->comment('资源');
            $table->tinyInteger('resource_type')->comment('资源类型')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('program');
    }
}
