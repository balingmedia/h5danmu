<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/programs','Api\ProgramController@show');
Route::get('/programs/sort','Api\ProgramController@showSort')->name('sort');
Route::get('/programs/{program?}','Api\ProgramController@showOne')->name('details');
Route::post('/programs/vote/{program?}','Api\ProgramController@vote')->name('vote');
Route::get('/votes','Api\VoteController@create')->name('zan');
