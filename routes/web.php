<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('show/index', 'Show\IndexController@index');
Route::get('phone/redirect', 'Phone\IndexController@redirect')->name('redirect');
Route::get('phone/index', 'Phone\IndexController@index');

Route::get('test', function (){
    return view('phone.index');
})->name('test');


//
//Route::get('test2', function (){
//    $data = \Illuminate\Support\Facades\DB::table('record')->get(['id','name','score']);
//    echo 'id,name,score   <br/>';
//    foreach ($data as $k => $v){
//        $v->name = base64_decode($v->name);
//        echo $v->id.',';
//        echo $v->name.',';
//        echo $v->score;
//        echo '<br/>';
//    }
//});