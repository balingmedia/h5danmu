if ("WebSocket" in window)
{
    // var ws = new WebSocket("wss://www.1yema.club:9500");
    var ws = new WebSocket("ws://barrage.balingmedia.com:9500");
    var allLen = 100
    ws.onopen = function()
    {
        var data = {
            openid : openid,
            nickname : nickname,
            // headimgurl:headimgurl,
            type:1
        };
        ws.send(JSON.stringify(data));
    };

    ws.onmessage = function (evt)
    {
        var data = JSON.parse(evt.data);
        // console.log(data);
        if(data.message){
            $(".page1").append(message(data.nickname,data.message));
        }else if(data.code && data.code == 1){
            $("#now").html('【暂无节目播放】');
        }else if(data.code && data.code == 2){
            $("#now").html(""+data.nowtype+" : "+data.program+"");
            $("#next").html(""+data.nexttype+" : "+data.next+"");
            $("#image").attr("src",data.image);
            window.sessionStorage["program_id"] = data.id;
            var content =  $("#toupiao"+data.id).text()
            console.log(data)
            if(data.type_id == 2){
                $(".zan-btn").hide();
            }else if(content == '已投'){
                $(".zan-btn").show();
                $(".zan-btn").html('已投');
                $(".zan-btn").css('pointer-events','none');
            }else{
                $(".zan-btn").show();
                $(".zan-btn").html('投票');
                $(".zan-btn").css('pointer-events','');
            }
        }else if(data.code && data.code == 3){
            var quelist = queue.quere()
            var offindex1 = quelist.findIndex(mitem => mitem == data.fd); //返回当前选择的  那一项规格 数据
            if(offindex1<0) {
                queue.push(data.fd);
            }
        }else{
            $(".page1").append(welcome(data.nickname));
        }

        var len = $('.mesline').length;
        if( len > allLen){
            var diffLen = len - allLen;
            // for(var i = 0; i<diffLen; i++){
            $('.mesline:lt('+diffLen+')').remove()
            // }
        }

        if(tabItemIndex == 1){
            $('#message')[0].scrollTop =$('#message')[0].scrollHeight;
        }
    };

    ws.onclose = function()
    {
        alert('直播室失联了');
        location.reload()
    };

    ws.onerror = function()
    {
        alert('直播室歇菜了');
        location.reload()
    };

    $("#send").click(function () {
        if(!$("#content").val()){
            return false;
        }

        console.log(window.sessionStorage["program_id"])
        if(window.sessionStorage["program_id"] == '-1'){
            layers.msg('演出暂未开始');
            return false;
        }
        var data = {
            openid : openid,
            nickname : nickname,
            // headimgurl:headimgurl,
            message:$("#content").val(),
            type:2
        };
        ws.send(JSON.stringify(data));
        $(".page1").append(message(nickname,$("#content").val()));
        $('#message')[0].scrollTop =$('#message')[0].scrollHeight;
        $("#content").val('');
        $("#content").focus();
        var len = $('.mesline').length;
        if( len > allLen){
            var diffLen = len - allLen;
            // for(var i = 0; i<diffLen; i++){
            $('.mesline:lt('+diffLen+')').remove()
            // }
        }
    });





}else{
    alert("您的浏览器不支持 WebSocket!");
}


function message(name,mes) {
    return  "<p class='user-word mesline'><span class='name'>"+name+" :</span>"+mes+"</p>";
}

function welcome(name) {
    return "<div class='welcome mesline'>" +
            "欢迎<span>"+name+"</span>光临直播间" +
            "</div>";
}


setInterval(function(){
    if(queue.quere().length>0) {
        for (var i = 0; i < queue.size(); i++) {
            //$(".journal-reward").click();
            $("#autoClick").trigger("click");
            queue.pop();
        }
    }
},1300)
