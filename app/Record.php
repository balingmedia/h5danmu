<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Record extends Model
{
    //
    public function isExists($id,$program)
    {
        return $this->where('openid',$id)->where('program_id',$program)->exists();
    }
}
