<?php

namespace App\Http\Controllers\phone;

use App\Http\Controllers\Controller;
use App\Program;
use App\Record;
use App\Traits\Wechat;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Redis;

class IndexController extends Controller
{
    use Wechat;
    //
    public function redirect()
    {
        return redirect()->away('https://open.weixin.qq.com/connect/oauth2/authorize?'.
            'appid='.env('APP_ID').
            '&redirect_uri='.env('APP_URL').'/phone/index'.
            '&response_type=code&scope=snsapi_userinfo&state=STATE#wechat_redirect');
    }

    /**
     * 手机端首页
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function index(Program $program)
    {
        $response = $this->getWebAccessToken();
        $body = (new Client())->send(
            new Request(
                'GET',
                'https://api.weixin.qq.com/sns/userinfo?access_token='.$response['access_token'].'&openid='.$response['openid'].'&lang=zh_CN'
            ),
            ['timeout' => 3]
        )->getBody()->getContents();

        session(['openid' => $response['openid']]);
        if(Redis::exists('play') && $program->isExists(Redis::get('play'))){
            $now = $program->where('id',Redis::get('play'))->first()->toArray();
            $next = $program->isExists($now['id']+1);
            if($next){
                $next = $program->where('id',$now['id']+1)->first();
            }else{
                $next = ['id' => 0,'program_name' => '闭幕','program_type' => '闭幕','resource_phone' => '/static/img/B.png'];
            }
        }else{
            $now = ['id' => -1,'program_name' => '开幕','program_type' => '开幕','resource_phone' => '/static/img/B.png'];
            $next = $program->limit(1)->first();
        }
        return view('phone.index',[
            'info' => json_decode($body,true),
            'program' => ['now' => $now,'next' => $next],
            'is_vote'   => (new Record())->isExists($response['openid'],$now['id']),
        ]);

    }



}
