<?php

namespace App\Http\Controllers\Api;

use App\Events\Vote;
use App\Vote as VoteModel;
use App\Program;
use App\Record;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProgramController extends Controller
{
    /**
     * 所有
     * @param Program $program
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Program $program)
    {
        return response()->json([
            'code'      =>      '1',
            'message'   =>      'success',
            'data'      =>      $program->get(),
        ]);
    }

    /**
     * 展示
     * @return \Illuminate\Http\JsonResponse
     */
    public function showSort()
    {
        return response()->json([
            'code'      =>      '1',
            'message'   =>      'success',
            'data'      =>      Program::with('vote')->with(['record' => function ($query) {
                $query->where('openid',session('openid'));
            }])->where('type_id',1)->get(['id','program_name']),
            'count'     =>      VoteModel::sum('count')
        ]);
    }


    public function vote($program,Record $record)
    {
        if($record->isExists(session('openid'),$program)){
            return response()->json([
                'code'      =>      '0',
                'message'   =>      'record exists',
            ]);
        }

        if(Program::find($program)->type_id == 2){
            return response()->json([
                'code'      =>      '2',
                'message'   =>      'program not vote',
            ]);
        }
		
        event(new Vote($program,session('openid')));
        return response()->json([
            'code'      =>      '1',
            'message'   =>      'vote success',
        ]);
    }

    public function showOne($program)
    {
        return response()->json([
            'code'      =>      '1',
            'message'   =>      'success',
            'data'      =>      (new Program())->where('id',$program)->first(['program_details']),
        ]);
    }
}
