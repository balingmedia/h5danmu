<?php

namespace App\Tools;

use App\Traits\WebSocket;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;

class SeparateUser
{
    use WebSocket;

    private $webSocket;

    private $allFd;

    private $userList;

    private $bigScreen;

    private $myfd;

    private $message;

    public function __construct($ws,$myfd,$message)
    {
        $this->webSocket = $ws;
        $this->myfd = $myfd;
        $this->message = $message;
        $this->allFd = Redis::smembers('chatroom');
        $this->bigScreen = $this->bigScreenFd();
        $this->userList = $this->allFd;
    }

    /**
     * 给用户发消息
     */
    public function sendToUser()
    {
        unset($this->message['openid']);
        //卸载掉自己
        $key = array_search($this->myfd,$this->userList);
        if($key !== false){
            unset($this->userList[$key]);
        }

        if(empty($this->userList)){
            return $this;
        }
        $message = json_encode($this->message);
        foreach ($this->userList as $value){
            //如果是存在的连接
            if($this->isEstablished($value)){
                $this->webSocket->push($value,$message);
            }else{
                //否则，移除该用户
                $this->removeUserList($value);
            }
        }
        Log::info('当前房间人数',[$this->getUserCount()]);
        Log::info('发送消息的人数',[count($this->userList)]);
        return $this;
    }

    /**
     * 单独给大屏幕发消息
     */
    public function sendToBigScreen()
    {
        if($this->bigScreen && $this->isEstablished($this->bigScreen)){
            //大屏幕不需要openid
            unset($this->message['openid'],$this->message['nickname']);
            if($this->isEstablished($this->bigScreen)){
                $this->webSocket->push($this->bigScreen,json_encode($this->message));
            }
        }
        return $this;
    }

    //提取大屏幕fd
    private function bigScreenFd()
    {
        $bigViewFd = false;
        if($this->bigScreenExists()) {
            $bigViewFd = Redis::get('bigscreen');
            $key = array_search(Redis::get('bigscreen'), $this->allFd);
            if($key !== false){
                unset($this->allFd[$key]);
            }
        }
        return $bigViewFd;
    }

    //大屏幕是否存在，并且是否是连接状态
    private function bigScreenExists()
    {
        return Redis::exists('bigscreen') && in_array(Redis::get('bigscreen'),$this->allFd);
    }

    /**
     * 判断连接是否存在
     * @param $fd
     * @return mixed
     */
    private function isEstablished($fd)
    {
        return $this->webSocket->isEstablished($fd);
    }

    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }
}