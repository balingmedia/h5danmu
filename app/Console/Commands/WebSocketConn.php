<?php

namespace App\Console\Commands;

use App\Events\Barrage;
use App\Events\Zan;
use App\Tools\SeparateUser;
use App\Traits\WebSocket;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;

class WebSocketConn extends Command
{
    use WebSocket;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'swoole:websocket';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'swoole:websocket';

    /**
     * new swoole
     * @var
     */
    private $webScoket;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * 是否需要开启ssl
     */
    private function setSsl()
    {
        $this->webScoket->set([
            'worker_num' => env('SWOOLE_WORKER'),
//            'ssl_cert_file'=> __DIR__ . '/ssl/1_www.1yema.club_bundle.crt',
//            'ssl_key_file' => __DIR__ . '/ssl/2_www.1yema.club.key',
        ]);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->webScoket = new \swoole_websocket_server(
            env('SWOOLE_IP'),
            env('SWOOLE_PORT')
//            SWOOLE_PROCESS,
//            SWOOLE_SOCK_TCP | SWOOLE_SSL
        );

        if( env('SWOOLE_SSL') ){
            $this->setSsl();
        }

        $this->webScoket->on('open', function ($ws, $request) {
            //将用户的fd存入集合
            $this->addUserList($request->fd);
        });

        $this->webScoket->on('message', function ($ws, $frame) {
            $data = json_decode($frame->data,true);
            //验证身份
            $this->checkAuth($frame->fd,$data);
            // type=1 用户连接  =2 用户发消息  =3大屏幕连接  =4大屏幕发消息  =5用户点赞
            //不是大屏幕连接发送的消息时
            $separate = new SeparateUser($this->webScoket,$frame->fd,$data);
            switch ($data['type']){
                case 1:
                    $separate->sendToUser();
                    break;
                case 2:
                    $separate->sendToUser()->sendToBigScreen();
                    break;
                case 3:
                    if(!isset($data['id'])){
                        $this->closeConn($frame->fd);
                    }else{
                        Redis::set('play',$data['id']);
                        $separate->setMessage(['code' => 2,'program' => $data['programs'],'next' => $data['next'],'id' => $data['id'],'image' => $data['image'],'nowtype' => $data['nowtype'],'nexttype' => $data['nexttype'],'type_id' => $data['type_id']])->sendToUser();
                    }
                    break;
                case 4:
                    break;
                case 5:
//                    event(new Zan( $data ));
                    $separate->setMessage(['code' => 3,'fd'=>$frame->fd])->sendToUser();
                    break;
            }
        });

        $this->webScoket->on('close', function ($ws, $fd) {
            if( Redis::exists('bigscreen') && $fd == Redis::get('bigscreen') ){
                $separate = new SeparateUser($this->webScoket,$fd,['code' => 1]);
                $separate->sendToUser();
            }
            $this->removeUserList($fd);
        });

        $this->webScoket->start();
    }


    /**
     * 身份验证
     * @param $fd
     * @param $message
     */
    private function checkAuth($fd,$message)
    {
        //如果不存在openid 并且存在连接,断开
        if(!isset($message['openid']) && $this->isEstablished($fd)){
            $this->closeConn($fd); // false
        }
        //如果是大屏幕,直接覆盖fd,继续执行
        if($message['openid'] == env('SHOW_OPENID')){
            //覆盖之前将前一个屏幕端口
            if($this->isEstablished(Redis::get('bigscreen')) && $fd != Redis::get('bigscreen')){
                $this->closeConn(Redis::get('bigscreen'));
            }
            Redis::set('bigscreen',$fd);
        }
    }

    /**
     * 判断连接是否存在
     * @param $fd
     * @return mixed
     */
    private function isEstablished($fd)
    {
        return $this->webScoket->isEstablished($fd);
    }


    /**
     * 主动关闭
     * @param $fd
     * @return mixed
     */
    private function closeConn($fd)
    {
        return $this->webScoket->disconnect($fd);
    }


}

