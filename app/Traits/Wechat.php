<?php

namespace App\Traits;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

trait Wechat{

    public function getWebAccessToken()
    {
        //如果存在就直接返回
//        if(Cache::has('web_access_token')){
//            Log::info('web_access_token exists');
//            return Cache::get('web_access_token');
//        }
//
        //如果不存在，判断是否存在刷新
//        if(Cache::has('refresh_token')){
//            $response = $this->refreshToken();
//        }else{
//        }

        $response = $this->getAccessToken();
        $response = $this->checkResponse($response);
//        Cache::put('web_access_token',$response['access_token'],120);
//        Cache::put('refresh_token',$response['refresh_token'],21600);
        return ['access_token' => $response['access_token'],'openid' => $response['openid']];
    }

    /**
     * 检查响应内容
     * @param $body
     * @return mixed
     * @throws \Exception
     */
    private function checkResponse($body)
    {
        $response = json_decode($body,true);
        if(isset($response['errcode']) && ($response['errcode'] == '40029' || $response['errcode'] == '40163') ){
            header('location:'.'https://open.weixin.qq.com/connect/oauth2/authorize?'.
                'appid='.env('APP_ID').
                '&redirect_uri='.env('APP_URL').'/phone/index'.
                '&response_type=code&scope=snsapi_userinfo&state=STATE#wechat_redirect');
            exit();
        }

        if( !isset($response['access_token']) ){
            throw new \Exception($body);
        }
        return $response;
    }

    /**
     * 刷新token
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function refreshToken()
    {
        Log::info('refreshToken');
        $refreshToken = Cache::get('refresh_token');
        return (new Client())->send(
            new Request(
                'GET',
                'https://api.weixin.qq.com/sns/oauth2/refresh_token?'. 'appid='.env('APP_ID'). '&grant_type=refresh_token&refresh_token='.$refreshToken
            ),
            ['timeout' => 3]
        )->getBody()->getContents();
    }

    /**
     * 直接获取
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function getAccessToken()
    {
        Log::info('getAccessToken');
        return (new Client())->send(
            new Request(
                'GET',
                'https://api.weixin.qq.com/sns/oauth2/access_token?'. 'appid='.env('APP_ID'). '&secret='.env('APP_SECRET'). '&code='.request('code'). '&grant_type=authorization_code'
            ),
            ['timeout' => 3]
        )->getBody()->getContents();
    }

}
