<?php

namespace App\Traits;

use Illuminate\Support\Facades\Redis;

trait WebSocket
{
    /**
     * 给节目集合新增成员
     * @param $fd
     */
    public function addUserList($fd)
    {
        Redis::sadd('chatroom',$fd);
    }

    /**
     * 给节目集合移除成员
     * @param $fd
     */
    public function removeUserList($fd)
    {
        Redis::srem('chatroom',$fd);
    }

    /**
     * 获得人数
     * @return mixed
     */
    public function getUserCount()
    {
        return Redis::smembers('chatroom');
    }
}