<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    //
    public function isExists($id)
    {
        return $this->where('id',$id)->exists();
    }

    public function vote()
    {
        return $this->hasOne(Vote::class,'program_id');
    }

    public function record()
    {
        return $this->hasMany(Record::class,'program_id');
    }
}
