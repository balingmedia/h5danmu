<?php

namespace App\Listeners;

use App\Events\Zan;
use App\Program;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;

class ZanListen implements ShouldQueue
{
    public $tries = 1;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Zan  $event
     * @return void
     */
    public function handle(Zan $event)
    {
        $request = $event->request;

        Log::info('点赞开始',[$request]);
        //如果缓存中正在播放节目和点赞的节目不符合
        if(Redis::get('play') != $request['program_id']){
            Log::info('和当前播放的节目不对',[$request]);
            return true;
        }

        //如果缓存存在，直接自增对应的点赞数
        if(Redis::exists('program_'.$request['program_id'])){
            Log::info('缓存存在，相加',[$request]);
            Redis::incrby('program_'.$request['program_id'],$request['count']);
            return true;
        }

        //如果缓存不存在，查看节目是否存在
        if( !(new Program())->isExists( $request['program_id'] )){
            Log::info('节目不存在',[$request]);
            return true;
        }

        Log::info('第一次，创建',[$request]);
        Redis::setnx('program_'.$request['program_id'],$request['count']);
    }
}
