<?php

namespace App\Listeners;

use App\Record;
use App\Vote;
use App\Events\Vote as VoteEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;

class VoteListen implements ShouldQueue
{

    public $tries = 1;

    private $vote;

    public $now;
	
	public $queue = 'vote';
	
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Vote $vote)
    {
        //
        $this->vote = $vote;
        $this->now = date('Y-m-d H:i:s',time());
    }

    /**
     * Handle the event.
     *
     * @param  Vote  $event
     * @return void
     */
    public function handle(VoteEvent $event)
    {
        //
        DB::beginTransaction();

        if(!$this->vote->isExists($event->program)){
            $result = DB::table('votes')->insert([
                'count'     =>      1,
                'created_at'    =>      $this->now,
                'updated_at'    =>      $this->now,
                'program_id'    =>      $event->program,
            ]);
        }else{            
			$result = DB::table('votes')->where('program_id',$event->program)->increment('count',1);

        }

        if(!$result){
            DB::rollBack();
            return true;
        }

        $result = DB::table('records')->insert([
            'openid'    =>          $event->openid,
            'created_at'    =>      $this->now,
            'updated_at'    =>      $this->now,
            'program_id'    =>      $event->program,
        ]);

        if(!$result){
            DB::rollBack();
            return true;
        }

        DB::commit();
        return true;
    }
}
