<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    //
    public function isExists($id)
    {
        return $this->where('program_id',$id)->exists();
    }
}
